import sys
import capnp
import controller_capnp
client = capnp.TwoPartyClient("localhost:5923")
controller = client.bootstrap().cast_as(controller_capnp.Controller)
controller.step(int(sys.argv[1])).wait()
