#include <iostream>

#include "events.hpp"

using namespace caf;


CAF_BEGIN_TYPE_ID_BLOCK(serial, first_custom_type_id)
  CAF_ADD_TYPE_ID(serial, (DeviceEvent))
CAF_END_TYPE_ID_BLOCK(serial)


namespace actors {

behavior event_handler(event_based_actor* self) {
  return {
  [=](const DeviceEvent& event) {
    switch (event.type) {
    case SerialEvent::LogEntry:
      aout(self) << "Firmware: " << (char*)event.data << std::endl;
      break;
    case SerialEvent::OrientationChanged:
      aout(self) << "got orientation. len: " << event.len << std::endl;
      auto x = *reinterpret_cast<float*>(event.data);
      auto y = *reinterpret_cast<float*>(event.data + 4);
      auto z = *reinterpret_cast<float*>(event.data + 8);
      aout(self) << "x: " << x << " y: " << y << " z: " << z << std::endl;
      break;
    }
  }
};
}

behavior logger(event_based_actor* self) {
  return {
      [=](std::string message) { aout(self) << message << std::endl; },
  };
}

} // namespace actors
