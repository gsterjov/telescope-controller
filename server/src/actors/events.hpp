#include "caf/all.hpp"


enum class SerialEvent {
  LogEntry = 0x01,
  OrientationChanged = 0x02,
};


struct DeviceEvent {
  SerialEvent type;
  size_t len;
  uint8_t* data;
};


namespace actors {
caf::behavior event_handler(caf::event_based_actor* self);
caf::behavior logger(caf::event_based_actor* self);
}
