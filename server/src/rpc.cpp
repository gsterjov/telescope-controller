#include <capnp/ez-rpc.h>
#include "../protocols/controller.capnp.h"
#include "kj/async.h"

#include "rpc.hpp"


class ControllerImpl final: public Controller::Server {
public:
  ControllerImpl(caf::blocking_actor* self, caf::actor notifier) {
    this->self = self;
    this->notifier = notifier;
  }

  kj::Promise<void> step(StepContext context) override {
    auto steps = context.getParams().getAmount();
    self->send(notifier, (int32_t)steps);
    return kj::READY_NOW;
  }

private:
  caf::blocking_actor* self;
  caf::actor notifier;
};


// A blocking actor that handles all rpc messages and sends them
// back into the actor system via the provided notifier actor
void rpc_server_listen(caf::blocking_actor* self, caf::actor notifier) {
    aout(self) << "Starting RPC server" << std::endl;

    capnp::EzRpcServer server(kj::heap<ControllerImpl>(self, notifier), "localhost", 5923);
    auto &waitScope = server.getWaitScope();
    kj::NEVER_DONE.wait(waitScope);
}
