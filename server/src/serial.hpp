#include <cstdlib>
#include <string>
#include <optional>
#include <tuple>
#include <memory>
#include <stdint.h>


class SerialWriter;
class SerialReader;


class Serial {
public:
  enum class ErrorCode {
    PermissionDenied, // EACCESS
    Busy,             // EBUSY
    NoDevice,         // ENXIO, ENODEV
    PartialWrite,
    Other,
  };

  class Error {
  public:
    ErrorCode code;
    std::string message;
    Error(ErrorCode code);
  };

  int fd;

  std::optional<Error> open(std::string path);
  std::optional<Error> close();

  std::optional<Error> read(uint8_t* buf, size_t len);
  std::optional<Error> write(uint8_t* buf, size_t len);

private:
  bool opened;
  int epoll_fd;

  std::optional<Error> configure();
  std::optional<Error> attach_epoll();
};


class SerialReader {
  public:
    SerialReader(std::shared_ptr<Serial> serial);
    std::optional<Serial::Error> read(uint8_t* buf, size_t len);

  private:
    std::shared_ptr<Serial> serial;
};


class SerialWriter {
  public:
    SerialWriter(std::shared_ptr<Serial> serial);
    std::optional<Serial::Error> write(uint8_t* buf, size_t len);

  private:
    std::shared_ptr<Serial> serial;
};


std::tuple<SerialWriter, SerialReader> serial_device_open(std::string path);
