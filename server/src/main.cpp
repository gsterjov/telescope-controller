#include <string>
#include <iostream>
#include <tuple>
#include <unistd.h>

#include "caf/all.hpp"

#include "caf/type_id.hpp"
#include "serial.hpp"
#include "rpc.hpp"

#include "actors/events.hpp"


using namespace caf;

struct DeviceCommand;


// we dont plan on using these across a network
CAF_ALLOW_UNSAFE_MESSAGE_TYPE(SerialWriter);
CAF_ALLOW_UNSAFE_MESSAGE_TYPE(DeviceCommand);
CAF_ALLOW_UNSAFE_MESSAGE_TYPE(SerialEvent);
CAF_ALLOW_UNSAFE_MESSAGE_TYPE(DeviceEvent);

CAF_BEGIN_TYPE_ID_BLOCK(serial, first_custom_type_id)
  // CAF_ADD_TYPE_ID(serial, (SerialWriter))
  CAF_ADD_TYPE_ID(serial, (DeviceCommand))
  CAF_ADD_TYPE_ID(serial, (SerialEvent))
  CAF_ADD_TYPE_ID(serial, (DeviceEvent))
CAF_END_TYPE_ID_BLOCK(serial)


struct DeviceCommand {
  uint8_t type;
  size_t len;
  uint8_t* buffer;
};


class RotateClockwise {
public:
  RotateClockwise(uint32_t steps);
  uint32_t steps;
  DeviceCommand to_command();
};

RotateClockwise::RotateClockwise(uint32_t steps) {
  this->steps = steps;
}

DeviceCommand RotateClockwise::to_command() {
  DeviceCommand cmd;
  cmd.type = 0x01;
  cmd.len = 5;
  cmd.buffer = (uint8_t *)malloc(5);

  cmd.buffer[0] = 0x01;

  cmd.buffer[1] = steps & 0x000000ff;
  cmd.buffer[2] = (steps & 0x0000ff00) >> 8;
  cmd.buffer[3] = (steps & 0x00ff0000) >> 16;
  cmd.buffer[4] = (steps & 0xff000000) >> 24;

  return cmd;
}

void serial_reader(blocking_actor* self, SerialReader reader, const actor& notify) {
    aout(self) << "Reading serial messages" << std::endl;

    uint8_t command;
    uint16_t len;

    while (true) {
        aout(self) << "Reading event" << std::endl;
        // event type - 1 byte
        if (auto err = reader.read(&command, 1)) {
            aout(self) << err->message << std::endl;
            self->fail_state();
            break;
        }

        aout(self) << "Reading payload length" << std::endl;
        // event payload length - 2 bytes
        if (auto err = reader.read(reinterpret_cast<uint8_t*>(&len), 2)) {
            aout(self) << err->message << std::endl;
            self->fail_state();
            break;
        }

        aout(self) << "Reading payload" << std::endl;
        // event payload - N bytes
        auto payload = (uint8_t*) malloc(len);
        if (auto err = reader.read(payload, len)) {
            aout(self) << err->message << std::endl;
            self->fail_state();
            break;
        }

        DeviceEvent event = {(SerialEvent)command, len, payload};
        self->send(notify, event);
        // self->send(notify, reinterpret_cast<const char*>(payload));
    }

    // serial.close();
    // aout(self) << "Serial interface closed" << std::endl;
}


struct serial_writer_state {
    optional<SerialWriter> writer;
};


behavior serial_writer(stateful_actor<serial_writer_state>* self, SerialWriter writer) {
    self->state.writer = writer;
    return {
        // [=](put_atom, SerialWriter writer) { self->state.writer = writer; },
        [=](DeviceCommand cmd) {
          std::optional<Serial::Error> err = self->state.writer->write(cmd.buffer, cmd.len);
          if (err) {
            aout(self) << "ERROR" << std::endl;
            aout(self) << err->code << " - " << err->message << std::endl;
          }

          free(cmd.buffer);
          cmd.type = 0;
          cmd.len = 0;
          cmd.buffer = NULL;
        },
    };
}


behavior take_steps(event_based_actor* self, actor writer) {
  return {
    [=](int32_t steps) {
      RotateClockwise cmd = RotateClockwise(steps);
      self->send(writer, cmd.to_command());
    }
  };
}


class rpc_server_notifier : public event_based_actor {
public:
    rpc_server_notifier(actor_config& cfg, actor writer) : event_based_actor(cfg) {
      this->stepper = this->spawn(take_steps, writer);
    }

    behavior make_behavior() override {
      return {
        [=](int32_t steps) {
          send(stepper, steps);
        }
      };
    }

private:
    actor stepper;
};


void caf_main(actor_system& sys) {
    auto[tx, rx] = serial_device_open("/dev/ttyUSB0");

    auto logger = sys.spawn(actors::logger);
    auto event_handler = sys.spawn(actors::event_handler);
    auto reader = sys.spawn(serial_reader, std::move(rx), event_handler);
    auto writer = sys.spawn(serial_writer, std::move(tx));

    auto notifier = sys.spawn<rpc_server_notifier>(writer);
    auto rpc = sys.spawn(rpc_server_listen, notifier);
}


// NOTE: if the custom type isn't registered here you will get segfaults
// trying to send the type as a message and pull your hair out trying to
// track it down
CAF_MAIN(id_block::serial)
