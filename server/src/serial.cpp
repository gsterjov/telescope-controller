#include <cstdint>
#include <stdio.h>
#include <string.h>
#include <cstdlib>
#include <cerrno>

#include <errno.h>
#include <tuple>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/epoll.h>

#include "serial.hpp"


using std::optional;
using Error = Serial::Error;
using ErrorCode = Serial::ErrorCode;


Error::Error(ErrorCode code) {
  this->code = code;
  this->message = strerror(errno);
}


optional<Error> Serial::open(std::string path) {
    opened = false;

    fd = ::open(path.c_str(), O_RDWR | O_NOCTTY | O_NDELAY);
    if (fd == -1) {
        switch (errno) {
            case EACCES:
                return ErrorCode::PermissionDenied;
            case EBUSY:
                return ErrorCode::Busy;
            case ENXIO:
            case ENODEV:
                return ErrorCode::NoDevice;
            default:
                return ErrorCode::Other;
        }
    }

    if (fcntl(fd, F_SETFL, FNDELAY) == -1) {
        return ErrorCode::Other;
    }

    if (auto err = configure()) return err;
    if (auto err = attach_epoll()) return err;

    opened = true;
    return {};
}


optional<Error> Serial::close() {
    if (::close(fd) != 0) {
        return ErrorCode::Other;
    };
    opened = false;
    return {};
}


optional<Error> Serial::configure() {
    struct termios tty;
    if (tcgetattr(fd, &tty) == -1) {
        return ErrorCode::Other;
    }

    // configure the serial port
    tty.c_cflag = 0;
    tty.c_cflag &= ~PARENB; // disable parity
    tty.c_cflag &= ~CSTOPB; // one stop bit
    tty.c_cflag &= ~CSIZE;
    tty.c_cflag |= CS8; // 8 bit
    tty.c_cflag &= ~CRTSCTS; // disable flow control
    tty.c_cflag |= CREAD | CLOCAL; // allow reads

    // configure local modes
    tty.c_lflag = 0;
    tty.c_lflag &= ~ICANON; // disable canonical mode
    tty.c_lflag &= ~ECHO; // disable echo
    tty.c_lflag &= ~ISIG; // disable signal chars

    // configure input modes
    tty.c_iflag = 0;
    tty.c_iflag &= ~(IXON | IXOFF | IXANY); // disable all flow control
    tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL); // raw byte data

    // configure output modes
    tty.c_oflag = 0;
    tty.c_oflag &= ~OPOST; // raw byte output
    tty.c_oflag &= ~ONLCR; // disable carriage return conversion

    // configure read() timeouts
    tty.c_cc[VTIME] = 0; // no blocking
    tty.c_cc[VMIN] = 0;

    // configure baud rate
    if (cfsetispeed(&tty, B9600) == -1) {
        return ErrorCode::Other;
    }
    if (cfsetospeed(&tty, B9600) == -1) {
        return ErrorCode::Other;
    };

    // this succeeds if any attribute is successfully set. which means
    // after setting the configuration has to be verified after. we
    // don't bother with that for now
    if (tcsetattr(fd, TCSANOW, &tty) == -1) {
        return ErrorCode::Other;
    }

    return {};
}


optional<Error> Serial::attach_epoll() {
    struct epoll_event event;

    epoll_fd = epoll_create1(0);
    if (epoll_fd == -1) {
        return ErrorCode::Other;
    }

    event.events = EPOLLIN;
    event.data.fd = fd;

    if (epoll_ctl(epoll_fd, EPOLL_CTL_ADD, fd, &event) == -1) {
        return ErrorCode::Other;
    }

    return {};
}


optional<Error> Serial::read(uint8_t* buf, size_t len) {
    size_t total_bytes_read = 0;
    struct epoll_event event;

    while (total_bytes_read < len) {
        int event_count = epoll_wait(epoll_fd, &event, 1, 2000);

        // ignore timeouts and read indefinitely until we fill the
        // buffer or run into an error
        if (event_count > 0) {
            int bytes_read = ::read(event.data.fd, buf, len - total_bytes_read);
            buf += bytes_read;
            total_bytes_read += bytes_read;
        }
        else if (event_count == -1) {
            return ErrorCode::Other;
        }
    }

    return {};
}


optional<Error> Serial::write(uint8_t* buf, size_t len) {
    int bytes_written = ::write(fd, buf, len);
    if (bytes_written == -1) return ErrorCode::Other;
    if (bytes_written < len) return ErrorCode::PartialWrite;
    return {};
}


std::tuple<SerialWriter, SerialReader> serial_device_open(std::string path) {
    std::shared_ptr<Serial> ptr = std::make_shared<Serial>();

    if (auto err = ptr->open(path)) {
        throw err;
    }

    return std::make_tuple(SerialWriter(ptr), SerialReader(ptr));
}


SerialReader::SerialReader(std::shared_ptr<Serial> serial) {
    this->serial = serial;
}

std::optional<Error> SerialReader::read(uint8_t* buf, size_t len) {
    return this->serial->read(buf, len);
}


SerialWriter::SerialWriter(std::shared_ptr<Serial> serial) {
    this->serial = serial;
}

std::optional<Error> SerialWriter::write(uint8_t* buf, size_t len) {
    return this->serial->write(buf, len);
}
