# Telescope Controller
An experimental project to create a automatic tracking telescope mount using an arduino and friends.
The main goal is to have the ability to control where the telescope points on a laptop feeding coordinates
that comes from a program like stellarium. The ability to track an object would be nice but might need
some iteration to ensure the motors and gears driving the movement are precise and smooth enough.

## Current goal
- [x] An arduino UNO board that receives commands via serial to take a specified number of motor steps
  - [x] A prototype with a small stepper motor using the `uln2003an` chip
  - [x] A prototype with the more powerful nema 17 stepper motor using the `L298` chip
  - [ ] Driving two stepepr motors with a Dual H-Bridge motor driver
- [ ] Port to FreeRTOS and drive motors concurrently

- [x] A daemon that connects to the controller via serial and provides a capn'proto interface
- [x] A CLI that can pass a step X command to the controller via serial
  - [ ] Make it send commands to the daemon
  - [x] Stream logs that are received by the daemon

- [x] Develop a simple serial protocol for commands, responses, and logs
  - [ ] A binary protocol supporting primitive types
  - [x] [TLV format](https://en.wikipedia.org/wiki/Type%E2%80%93length%E2%80%93value). Not unlike the [protobuf message format](https://developers.google.com/protocol-buffers/docs/encoding)


## Compiling

### uln2003an
``` sh
cd uln2003an
arduino-cli compile -b arduino:avr:uno
arduino-cli upload -p /dev/ttyUSB0 -b arduino:avr:uno ./
```

### CLI
``` sh
cd cli
meson setup build
cd build
meson compile
```

## Usage
To go forward 100 steps
``` sh
./controller 100
```
