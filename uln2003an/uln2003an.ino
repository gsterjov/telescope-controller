#define A 2
#define B 3
#define C 4
#define D 5


struct Command {
  enum command_t {
    RotateClockwise = 0x01,
  };
};


void setup() {
  pinMode(A, OUTPUT);
  pinMode(B, OUTPUT);
  pinMode(C, OUTPUT);
  pinMode(D, OUTPUT);

  // wait until serial port initialisation finishes
  Serial.begin(9600);
  while (!Serial) {}
}


void loop() {
  read_command();
}

void rotate_clockwise(int steps) {
  for (int i = 0; i < steps; ++i) {
    short_step();
  }
}

void read_command() {
  while (Serial.available() > 4) {
    Command::command_t cmd = Serial.read();

    switch (cmd) {
      case Command::RotateClockwise:
        char bytes[4] = {0};
        size_t bytes_read = Serial.readBytes(bytes, 4);

        long steps = 0;
        steps += bytes[3] << 24;
        steps += bytes[2] << 16;
        steps += bytes[1] << 8;
        steps += bytes[0];

        rotate_clockwise(steps);
        Serial.write(0x01);

        String payload = "Steps finished";
        unsigned int len = payload.length();

        Serial.write((uint8_t*)&len, 2);
        Serial.write(payload.c_str(), len);
        break;
      default:
        break;
        // Serial.println("Unknown command");
    }
  }
}


void write(int a, int b, int c, int d) {
  digitalWrite(A, a);
  digitalWrite(B, b);
  digitalWrite(C, c);
  digitalWrite(D, d);
}

void short_step() {
  write(1, 1, 0, 0);
  delay(5);
  write(0, 1, 1, 0);
  delay(5);
  write(0, 0, 1, 1);
  delay(5);
  write(1, 0, 0, 1);
  delay(5);
}

void long_step() {
  write(1, 0, 0, 0);
  delay(5);

  write(1, 1, 0, 0);
  delay(5);

  write(0, 1, 0, 0);
  delay(5);

  write(0, 1, 1, 0);
  delay(5);

  write(0, 0, 1, 0);
  delay(5);

  write(0, 0, 1, 1);
  delay(5);

  write(0, 0, 0, 1);
  delay(5);

  write(1, 0, 0, 1);
  delay(5);
}
