#define A 2
#define B 3
#define C 4
#define D 5


void setup() {
  pinMode(A, OUTPUT);
  pinMode(B, OUTPUT);
  pinMode(C, OUTPUT);
  pinMode(D, OUTPUT);
}


void loop() {
  long_step();
  delay(5);
}


void write(int a, int b, int c, int d) {
  digitalWrite(A, a);
  digitalWrite(B, b);
  digitalWrite(C, c);
  digitalWrite(D, d);
}

void short_step() {
  write(1, 1, 0, 0);
  delay(5);
  write(0, 1, 1, 0);
  delay(5);
  write(0, 0, 1, 1);
  delay(5);
  write(1, 0, 0, 1);
  delay(5);
}

void long_step() {
  write(1, 0, 0, 0);
  delay(5);

  write(1, 1, 0, 0);
  delay(5);

  write(0, 1, 0, 0);
  delay(5);

  write(0, 1, 1, 0);
  delay(5);

  write(0, 0, 1, 0);
  delay(5);

  write(0, 0, 1, 1);
  delay(5);

  write(0, 0, 0, 1);
  delay(5);

  write(1, 0, 0, 1);
  delay(5);
}
