#include "serial_events.h"

// these are still experimental and not all that safe. the implementation
// attempts to flush out a nice paradigm to make the firmware event based
// and play nicely with actor frameworks
namespace Events {

LogEntry::LogEntry(String entry) { _entry = entry; }

EventData LogEntry::to_data() {
  return {_entry.length(), reinterpret_cast<const uint8_t *>(_entry.c_str())};
}

OrientationChanged::OrientationChanged(float x, float y, float z) {
  _x = x;
  _y = y;
  _z = z;
}

EventData OrientationChanged::to_data() { return {12, (uint8_t *)&_x}; }

} // namespace Events
