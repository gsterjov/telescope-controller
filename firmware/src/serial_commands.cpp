#include "serial_commands.h"


namespace Commands {
    bool RotateClockwise::parse(uint8_t* data) {
        _steps = 0;
        _steps += data[3] << 24;
        _steps += data[2] << 16;
        _steps += data[1] << 8;
        _steps += data[0];

        return true;
    }

    Command::command_t parse(uint8_t data) {
      auto cmd = (Command::command_t)data;
      return cmd;
    }
}



