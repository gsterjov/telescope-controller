#include <Arduino.h>
#include <SPI.h>

#include <Arduino_FreeRTOS.h>
#include "portmacro.h"
#include "queue.h"

#include <Adafruit_BNO055.h>

#include "portable.h"
#include "serial_commands.h"
#include "serial_events.h"

#define A 2
#define B 3
#define C 4
#define D 5

Adafruit_BNO055 bno = Adafruit_BNO055(55);


void TaskWriteEvent(void* pvParameters);
void TaskCommandRead(void* pvParameters);
void TaskCommandExecute(void* pvParameters);
void TaskOrientationRead(void* pvParameters);


struct CommandMessage {
    Command::command_t type;
    union {
        Commands::RotateClockwise rotate_clockwise;
    };
};

// 5 bytes
struct EventMessage {
    SerialEvent::event_t type;
    EventData event;
};


struct SerialChannel {
    QueueHandle_t rx;
    QueueHandle_t tx;
};



void setup() {
    pinMode(A, OUTPUT);
    pinMode(B, OUTPUT);
    pinMode(C, OUTPUT);
    pinMode(D, OUTPUT);

    // wait until serial port initialisation finishes
    Serial.begin(9600);
    while (!Serial) {}

    // initialise the 9dof sensor
    if (!bno.begin()) {
        Serial.print("BNO055 not detected");
    }

    delay(1000);
    bno.setExtCrystalUse(true);


    auto cmd_queue = xQueueCreate(5, sizeof(CommandMessage));
    auto ev_queue = xQueueCreate(5, sizeof(EventMessage));

    auto channel = new SerialChannel { cmd_queue, ev_queue };

    xTaskCreate(TaskWriteEvent, "WriteEvent", 128, ev_queue, 2, NULL);
    // xTaskCreate(TaskOrientationRead, "OrentationRead", 128, ev_queue, 2, NULL);

    xTaskCreate(TaskCommandRead, "CommandRead", 128, channel, 2, NULL);
    xTaskCreate(TaskCommandExecute, "CommandExecute", 128, channel, 2, NULL);
}


void write_event(const EventData& event, SerialEvent::event_t type) {
    Serial.write(type);
    Serial.write((uint8_t*)&event.length, 2);
    Serial.write(event.data, event.length);
}


void get_orientation() {
    sensors_event_t event;
    bno.getEvent(&event);

    // send the current orientation as an event
    auto ev = Events::OrientationChanged(
        event.orientation.x,
        event.orientation.y,
        event.orientation.z
    );
    write_event(ev.to_data(), SerialEvent::OrientationChanged);
}



void write(int a, int b, int c, int d) {
    digitalWrite(A, a);
    digitalWrite(B, b);
    digitalWrite(C, c);
    digitalWrite(D, d);
}

void short_step() {
    write(1, 1, 0, 0);
    vTaskDelay(1);
    write(0, 1, 1, 0);
    vTaskDelay(1);
    write(0, 0, 1, 1);
    vTaskDelay(1);
    write(1, 0, 0, 1);
    vTaskDelay(1);
}

void long_step() {
    write(1, 0, 0, 0);
    vTaskDelay(1);

    write(1, 1, 0, 0);
    vTaskDelay(1);

    write(0, 1, 0, 0);
    vTaskDelay(1);

    write(0, 1, 1, 0);
    vTaskDelay(1);

    write(0, 0, 1, 0);
    vTaskDelay(1);

    write(0, 0, 1, 1);
    vTaskDelay(1);

    write(0, 0, 0, 1);
    vTaskDelay(1);

    write(1, 0, 0, 1);
    vTaskDelay(1);
}



void rotate_clockwise(int steps) {
    for (int i = 0; i < steps; ++i) {
        short_step();
    }
}

void loop() {}


void TaskWriteEvent(void* pvParameters) {
    auto ev_queue = (QueueHandle_t) pvParameters;

    for (;;) {
        EventMessage message;
        xQueueReceive(ev_queue, &message, portMAX_DELAY);
        write_event(message.event, message.type);
    }
}


void TaskOrientationRead(void* pvParameters) {
    auto ev_queue = (QueueHandle_t) pvParameters;

    for (;;) {
        auto ev = Events::OrientationChanged(1, 2, 3);
        auto message = EventMessage { SerialEvent::OrientationChanged, ev.to_data() };
        xQueueSendToBack(ev_queue, (void*)&message, portMAX_DELAY);

        vTaskDelay(100);
    }
}


void TaskCommandRead(void* pvParameters) {
    auto channel = (SerialChannel*) pvParameters;

    for (;;) {
        Command::command_t cmd = Commands::parse(Serial.read());

        // we read the amount of bytes we expect which will block
        // the task if it's not available. this allows us to parse
        // the command payload without needing a payload length and
        // buffering the read input
        switch (cmd) {
            case Command::RotateClockwise:
                auto rotate = Commands::RotateClockwise();
                uint8_t data[4] = {0};
                Serial.readBytes(data, 4);

                rotate.parse(data);

                auto message = CommandMessage { cmd };
                message.rotate_clockwise = rotate;
                xQueueSendToBack(channel->rx, (void*)&message, portMAX_DELAY);
                break;
        }
    }
}


void TaskCommandExecute(void* pvParameters) {
    auto channel = (SerialChannel*) pvParameters;

    for (;;) {
        CommandMessage message;
        xQueueReceive(channel->rx, &message, portMAX_DELAY);


        switch (message.type) {
            case Command::RotateClockwise:
                auto steps = message.rotate_clockwise.steps();
                rotate_clockwise(steps);

                auto ev = new Events::LogEntry("Steps finished");
                auto msg = new EventMessage { SerialEvent::LogEntry, ev->to_data() };
                xQueueSendToBack(channel->tx, msg, portMAX_DELAY);
                break;
        }
    }
}
