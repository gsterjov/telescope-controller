#include "stdint.h"

struct Command {
    enum command_t {
        RotateClockwise = 0x01,
    };
};


struct Event {
    enum event_t {
        LogEntry = 0x01,
        OrientationChanged = 0x02,
    };
};


namespace Commands {
    class RotateClockwise {
    public:
        // data must be exactly 4 bytes in length
        bool parse(uint8_t* data);
        uint32_t steps() { return _steps; };

    private:
        uint32_t _steps;
    };

    Command::command_t parse(uint8_t data);
}
