#include <Arduino.h>
#include <stdint.h>

struct SerialEvent {
  enum event_t {
    LogEntry = 0x01,
    OrientationChanged = 0x02,
  };
};

struct EventData {
  uint16_t length;
  const uint8_t* data; // 2 bytes
};

namespace Events {

class LogEntry {
public:
  LogEntry(String entry);
  EventData to_data();

private:
  String _entry;
};

class OrientationChanged {
public:
  OrientationChanged(float x, float y, float z);
  EventData to_data();

private:
  float _x;
  float _y;
  float _z;
};

} // namespace Events
