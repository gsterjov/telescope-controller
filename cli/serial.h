class Serial {
public:
    void open();
    void close();

    void write(unsigned char bytes[], size_t len);
    int read(char* buf, size_t len);

private:
    int fd;
    int epoll_fd;
};
