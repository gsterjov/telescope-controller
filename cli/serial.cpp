#include <stdio.h>
#include <string.h>
#include <cstdlib>

#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/epoll.h>

#include "serial.h"


void Serial::open() {
    fd = ::open("/dev/ttyUSB0", O_RDWR | O_NOCTTY | O_NDELAY);
    if (fd < 0) {
        fprintf(stderr, "Failed to open serial port: %s\n", strerror(errno));
        return;
    }
    fcntl(fd, F_SETFL, FNDELAY);

    struct epoll_event event;
    epoll_fd = epoll_create1(0);
    if (epoll_fd == -1) {
        fprintf(stderr, "Failed to create epoll file descriptor\n");
        return;
    }
    printf("epoll_fd: %i\n", epoll_fd);

    event.events = EPOLLIN;
    event.data.fd = fd;

    if (epoll_ctl(epoll_fd, EPOLL_CTL_ADD, fd, &event)) {
        fprintf(stderr, "Failed to add file descriptor to epoll\n");
        return;
    }

    struct termios tty;
    if (tcgetattr(fd, &tty) != 0) {
        fprintf(stderr, "Failed to get serial port configuration: %s\n", strerror(errno));
        return;
    }

    // configure the serial port
    tty.c_cflag = 0;
    tty.c_cflag &= ~PARENB; // disable parity
    tty.c_cflag &= ~CSTOPB; // one stop bit
    tty.c_cflag &= ~CSIZE;
    tty.c_cflag |= CS8; // 8 bit
    tty.c_cflag &= ~CRTSCTS; // disable flow control
    tty.c_cflag |= CREAD | CLOCAL; // allow reads

    // configure local modes
    tty.c_lflag = 0;
    tty.c_lflag &= ~ICANON; // disable canonical mode
    tty.c_lflag &= ~ECHO; // disable echo
    tty.c_lflag &= ~ISIG; // disable signal chars

    // configure input modes
    tty.c_iflag = 0;
    tty.c_iflag &= ~(IXON | IXOFF | IXANY); // disable all flow control
    tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL); // raw byte data

    // configure output modes
    tty.c_oflag = 0;
    tty.c_oflag &= ~OPOST; // raw byte output
    tty.c_oflag &= ~ONLCR; // disable carriage return conversion

    // configure read() timeouts
    tty.c_cc[VTIME] = 0; // no blocking
    tty.c_cc[VMIN] = 0;

    // configure baud rate
    cfsetispeed(&tty, B9600);
    cfsetospeed(&tty, B9600);

    if (tcsetattr(fd, TCSANOW, &tty) != 0) {
        fprintf(stderr, "Failed to set serial port configuration: %s\n", strerror(errno));
    }
}


void Serial::close() {
    if (::close(epoll_fd) != 0) {
        fprintf(stderr, "Failed to close epoll file descriptor\n");
        return;
    }

    ::close(fd);
}


void Serial::write(unsigned char bytes[], size_t len) {
    ::write(fd, bytes, len);
}

int Serial::read(char* buf, size_t len) {
    size_t total_bytes_read = 0;
    struct epoll_event event;

    while (total_bytes_read < len) {
        int event_count = epoll_wait(epoll_fd, &event, 1, 2000);
        if (event_count > 0) {
            int bytes_read = ::read(event.data.fd, buf, len - total_bytes_read);
            buf += bytes_read;
            total_bytes_read += bytes_read;
        }
        else {
            fprintf(stderr, "epoll timed out: %i\n", event_count);
            return -1;
        }
    }

    return total_bytes_read;
}
