#include "libs/argparse/argparse.hpp"
#include <stdexcept>

#include "serial.h"


void take_steps(int steps) {
    std::cout << "Taking " << steps << " steps" << std::endl;

    Serial serial = Serial();
    serial.open();

    // unsigned char command[] = { 0x01, 0x00, 0x00, 0x00, 0x00 };
    // serial.write(command, sizeof(command));
    unsigned char command[1] = { 0x01 };
    serial.write(command, 1);

    unsigned char bytes[4] = {0};
    bytes[0] = steps & 0x000000ff;
    bytes[1] = (steps & 0x0000ff00) >> 8;
    bytes[2] = (steps & 0x00ff0000) >> 16;
    bytes[3] = (steps & 0xff000000) >> 24;

    for (auto val : bytes) printf("%d ", val);
    printf("\n");

    serial.write(bytes, 4);

    // char buf[255] = {0};
    // int ret = serial.read(buf, sizeof(buf));
    // std::cout << "ret: " << ret << std::endl;
    // std::cout << "buf: " << buf << std::endl;

    serial.close();
}


int main(int argc, char *argv[]) {
    argparse::ArgumentParser program("controller");

    program.add_argument("steps")
        .help("Go forward the provided amount of steps")
        .scan<'i', int>();

    try {
        program.parse_args(argc, argv);
    }
    catch (const std::runtime_error& err) {
        std::cerr << err.what() << std::endl;
        std::cerr << program;
        std::exit(1);
    }

    auto steps = program.get<int>("steps");
    take_steps(steps);

    return 0;
}
